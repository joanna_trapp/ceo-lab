﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ceo_lab.BL;
using ceo_lab.DAL;
using ceo_lab.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace ceo_lab.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUsersService _userService;
        private readonly AppSettings _appSettings;

        public UsersController(IUsersService userService, IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpGet]
        public IEnumerable<User> Get()
        {
            return _userService.GetAll();
        }
    }
}
