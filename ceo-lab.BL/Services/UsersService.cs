﻿using ceo_lab.BL.Helpers;
using ceo_lab.DAL;
using ceo_lab.DAL.Data;
using ceo_lab.DAL.Repositories;
using ceo_lab.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace ceo_lab.BL
{
    public class UsersService : IUsersService
    {
        private readonly AppSettings _appSettings;
        private readonly IUsersRepository _usersRepository;

        public UsersService(IOptions<AppSettings> appSettings, IUsersRepository usersRepository)
        {
            _appSettings = appSettings.Value;
            _usersRepository = usersRepository;
        }
        public string LoginUser(string login, string password, string hospital)
        {
            throw new NotImplementedException();
        }

        public User Authenticate(string login, string password)
        {
            var user = ProgramData.users.SingleOrDefault(x => x.Login == login && x.Password == password);

            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            return user.WithoutPassword();
        }

        public IEnumerable<User> GetAll()
        {
            return _usersRepository.GetAll();
        }

        public User GetById(int id)
        {
            return _usersRepository.GetById(id);
        }
    }
}
