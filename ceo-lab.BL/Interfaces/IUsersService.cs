﻿using ceo_lab.DAL;
using System.Collections.Generic;

namespace ceo_lab.BL
{
    public interface IUsersService
    {
        User Authenticate(string login, string password);
        IEnumerable<User> GetAll();
        User GetById(int id);
        string LoginUser(string login, string password, string hospital);
    }
}