﻿using ceo_lab.DAL.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ceo_lab.DAL.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        public IEnumerable<User> GetAll()
        {
            return ProgramData.users;
        }

        public User GetById(int id)
        {
            return ProgramData.users.SingleOrDefault(x => x.Id == id);
        }
    }
}
