﻿using System.Collections.Generic;

namespace ceo_lab.DAL.Repositories
{
    public interface IUsersRepository
    {
        IEnumerable<User> GetAll();
        User GetById(int id);
    }
}