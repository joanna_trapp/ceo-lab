﻿using System;

namespace ceo_lab.DAL
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Hospital { get; set; }
        public string Token { get; set; }
    }
}
